'''
  This program checks urls for specific requests. Requires Splash server https://github.com/scrapinghub/splash
  Code by Vladimir Smagin, 2018
  Mail: 21h@blindage.org
'''

import requests
import re
import mailer
from urllib.parse import quote
import json
import config
import sys, os, signal
import datetime

class C:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

def sendmail(a, b):
    pass

def sendmail2(subj, body):
    message = mailer.Message(From='robot@blindage.org',
                      To=config.mailaddr)
    message.Subject = subj
    message.Html = """<p>Urgent report message<br>
       %s""" % body

    sender = mailer.Mailer('localhost')
    try:
        sender.send(message)
    except:
        print(C.FAIL+"Can't send message to sysadmin!"+C.ENDC)

if (len(sys.argv)>1) and (sys.argv[1] == '-html'):
    useHTMLTags = True
else:
    useHTMLTags = False

searchUrls = config.searchForRequests['urls']
searchRegexps = config.searchForRequests['regexps']

if useHTMLTags:
    print('<h1>Request checker report</h1>', 'Started', datetime.datetime.now())
    print("<table width='100%' border='1'>")
else:
    print(datetime.datetime.now())

for searchUrl in searchUrls:
    if useHTMLTags:
        print('<tr>')
    
    #очистить кеш, проверить состояние.
    try:
        r = requests.get(config.searchServer + "/_ping").text
        #print("\t",r)
        r = requests.post(config.searchServer + "/_gc").text
        #print("\t",r)
    except:
        print(C.FAIL + "Splash server is down. It's time to panic!" + C.ENDC)
        sys.exit(1)

    try:
        if useHTMLTags:
            print('<td>', searchUrl, '</td>')
        else:
            print(C.HEADER+"Checking URL", searchUrl+C.ENDC)
        pageUrlRequest = config.searchServer+"/render.har?url="+ quote(searchUrl)# + "&wait=30"
        r = requests.get(pageUrlRequest)
    except:
        print(C.WARNING + "Splash server connection error" + C.ENDC)
        sys.exit(1)

    if int(r.status_code) is 200:
        answer = r.json()
        htmlString = ''
        for requestUrl in answer['log']['entries']:
            url = requestUrl['response']['url']
            for searchRegexp in searchRegexps:
                if (re.search(searchRegexp, url)):
                    if useHTMLTags:
                        htmlString += '<strong>'+searchRegexp+'</strong><br>'+url+'<br>'
                    else:
                        print(C.OKGREEN + "Found request match",  "\n\t", C.OKBLUE + url, 
                                "\n\t", searchRegexp + C.ENDC)
    else:
        if useHTMLTags:
            htmlString = '<font color=red>'+r.text+'</font>'
        else:
            print(C.WARNING + "Response code is not 200, got " + str(r.status_code) + C.ENDC)

    if useHTMLTags:
        print('<td>', htmlString,"</td></tr>")
        sys.stdout.flush()

if useHTMLTags:
    print("</table>")
    print('Finished:', datetime.datetime.now())
