searchServer = "http://127.0.0.1:8050"

searchForRequests = {
    'analyticpixels.net':
        ['/pixel.js', '/manager.js']
}

searchURLs = [
    'https://google.com/buy/auto/#edit/new/data',
    'http://twotowers.ru',
    'http://blindage.org/'
]